export const escapeCharForSearch = (str: string): string => {
  return str.toLowerCase().replace(/[?%\\_]/gi, function (x) {
    return '\\' + x;
  });
};

export enum EnumSort {
  ASC = 'ASC',
  DESC = 'DESC',
}

export enum RememberPassword {
  active = 1,
  inactive = 0,
}

export enum StatusPermission {
  ACTIVE = 1,
  INACTIVE = 0,
}
