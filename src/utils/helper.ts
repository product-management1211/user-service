export default function isEmptyObject(obj) {
  for (const property in obj) {
    return false;
  }
  return true;
}

export function serialize(data) {
  if (data.length > 0) {
    const serilizeData = [];
    data.forEach((record) => {
      serilizeData[record.id] = record;
    });
    return serilizeData;
  }
  return data;
}
