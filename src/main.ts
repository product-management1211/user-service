import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from './config/config.service';

async function bootstrap() {
  const app = await NestFactory.createMicroservice(AppModule, {
    options: {
      host: '0.0.0.0',
      port: new ConfigService().get('port'),
    },
  });
  await app.listen();
}
bootstrap();
