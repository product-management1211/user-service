import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserRoleSetting } from '../user-role-setting/user-role-setting.entity';
import { User } from '../user/user.entity';

@Entity({ name: 'user_roles' })
export class UserRole {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'int',
  })
  userId: number;

  @Column({
    type: 'int',
  })
  userRoleId: number;

  @Column({
    type: 'int',
  })
  departmentId: number;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @ManyToOne(() => User, (user) => user.userRoles)
  user: User;

  @ManyToOne(() => User, (userRoleSetting) => userRoleSetting.userRoles)
  userRole: UserRoleSetting;
}
