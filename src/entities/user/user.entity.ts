import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserDepartment } from '../user-department/user-department.entity';
import { UserRole } from '../user-role/user-role.entity';
import { DepartmentSettings } from '../department-setting/department-setting.entity';
import { UserRoleSetting } from '../user-role-setting/user-role-setting.entity';
import { Factory } from '../factory/factory.entity';
import { UserFactory } from '../user-factory/user-factory.entity';
import { UserWarehouse } from '../user-warehouse/user-warehouse.entity';
import * as bcrypt from 'bcrypt';

export enum status {
  ACTIVE = 1,
  IN_ACTIVE = 0,
}

@Entity({ name: 'users' })
export class User {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column({
    type: 'varchar',
    unique: true,
    length: 255,
    nullable: true,
  })
  email: string;

  @Column({
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  username: string;

  @Column({
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  fullName: string;

  @Column({
    type: 'varchar',
    length: 255,
    select: false,
  })
  password: string;

  @Column({
    type: 'integer',
  })
  companyId: number;

  @Column({
    type: 'varchar',
    unique: true,
    length: 9,
  })
  code: string;

  @Column({
    type: 'varchar',
    length: 20,
    nullable: true,
  })
  phone: string;

  @Column({
    type: 'enum',
    enum: status,
    default: status.ACTIVE,
  })
  status: number;

  @Column({
    type: 'date',
    nullable: true,
  })
  dateOfBirth: Date;

  @Column({
    type: 'varchar',
    length: 6,
    nullable: true,
  })
  otpCode: string;

  @Column({
    type: 'timestamptz',
    nullable: true,
  })
  expire: Date;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  @Column()
  @DeleteDateColumn()
  deletedAt?: Date;

  @BeforeInsert()
  async hashPassword() {
    const saltRounds = 10;
    this.password = await bcrypt.hash(this.password, saltRounds);
  }

  async validatePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }

  @ManyToMany(() => DepartmentSettings)
  @JoinTable({
    name: 'user_roles',
    joinColumn: {
      name: 'user_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'department_id',
      referencedColumnName: 'id',
    },
  })
  departmentSettings: DepartmentSettings[];

  @ManyToMany(() => UserRoleSetting)
  @JoinTable({
    name: 'user_roles',
    joinColumn: {
      name: 'user_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'user_role_id',
      referencedColumnName: 'id',
    },
  })
  userRoleSettings: UserRoleSetting[];

  @ManyToMany(() => Factory)
  @JoinTable({
    name: 'user_factories',
    joinColumn: {
      name: 'user_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'factory_id',
      referencedColumnName: 'id',
    },
  })
  factories: Factory[];

  @OneToMany(() => UserDepartment, (userDepartment) => userDepartment.user)
  userDepartments: UserDepartment[];

  @OneToMany(() => UserRole, (userRole) => userRole.user)
  userRoles: UserRole[];

  @OneToMany(() => UserFactory, (userFactory) => userFactory.user)
  userFactories: UserFactory[];

  @OneToMany(() => UserWarehouse, (userWarehouse) => userWarehouse.user)
  userWarehouses: UserWarehouse[];
}
