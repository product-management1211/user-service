import * as dotenv from 'dotenv';
dotenv.config();

export enum APIPrefix {
  Version = 'api/v1',
}
export const jwtConstants = {
  accessTokenSecret: process.env.JWT_ACCESS_TOKEN_SECRET,
  accessTokenExpiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRES_IN || 1800,
  refreshTokenSecret: process.env.JWT_RESFRESH_TOKEN_SECRET,
  refreshTokenExpiresIn: process.env.JWT_RESFRESH_TOKEN_EXPIRES_IN || 2000,
  refreshTokenExpiresMaxIn:
    process.env.JWT_RESFRESH_TOKEN_EXPIRES_MAX_IN || 432000,
};

export const DEFAULT_DATA = {
  USERS: [
    {
      code: '000000001',
      username: 'admin',
      password: '121199',
      email: 'hieu.tran@ekoios.vn',
      fullName: 'Tran Minh Hieu',
    },
  ],
  COMPANIES: [
    {
      code: '000000001',
      name: 'VTI SnP',
    },
  ],
  DEPARMENTS: [
    {
      name: 'SALE',
    },
    {
      name: 'Kho',
    },
  ],
};

export const SUPER_ADMIN = {
  code: '000000001',
  username: 'admin',
  password: 'snp1234567',
  email: 'admin@smartwms.vti.com.vn',
  fullName: 'Admin',
};
