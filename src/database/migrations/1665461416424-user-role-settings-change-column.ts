import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class userRoleSettingsChangeColumn1665461416424
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE user_role_settings DROP COLUMN code`);
    await queryRunner.addColumn(
      'user_role_settings',
      new TableColumn({
        name: 'code',
        type: 'varchar',
        length: '2',
        isNullable: false,
        isUnique: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE user_roles_settings DROP COLUMN code`);
    await queryRunner.addColumn(
      'user_role_settings',
      new TableColumn({
        name: 'code',
        type: 'varchar',
        length: '2',
        isNullable: true,
        isUnique: true,
      }),
    );
  }
}
