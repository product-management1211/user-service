import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class companyAddColumn1664254133244 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'companies',
      new TableColumn({
        name: 'description',
        type: 'varchar',
        isNullable: true,
        length: '255',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE companies DROP COLUMN description`);
  }
}
