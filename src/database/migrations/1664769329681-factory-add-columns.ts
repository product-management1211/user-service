import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class factoryAddColumns1664769329681 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'factories',
      new TableColumn({
        name: 'deleted_at',
        type: 'timestamptz',
        isNullable: true,
      }),
    );
    await queryRunner.addColumn(
      'factories',
      new TableColumn({
        name: 'status',
        default: 0,
        type: 'int',
      }),
    );
    await queryRunner.addColumn(
      'factories',
      new TableColumn({
        name: 'approver_id',
        type: 'int',
        isNullable: true,
      }),
    );
    await queryRunner.addColumn(
      'factories',
      new TableColumn({
        name: 'approved_at',
        type: 'timestamptz',
        isNullable: true,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER TABLE factories DROP COLUMN deleted_at');
    await queryRunner.query('ALTER TABLE factories DROP COLUMN status');
    await queryRunner.query('ALTER TABLE factories DROP COLUMN approver_id');
    await queryRunner.query('ALTER TABLE factories DROP COLUMN approved_at');
  }
}
