import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class companyAddColumns1664262629239 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('companies', [
      new TableColumn({
        name: 'status',
        default: 0,
        type: 'int',
      }),
      new TableColumn({
        name: 'approver_id',
        type: 'int',
        isNullable: true,
      }),
      new TableColumn({
        name: 'approved_at',
        type: 'timestamptz',
        isNullable: true,
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER TABLE companies DROP COLUMN status');
    await queryRunner.query('ALTER TABLE companies DROP COLUMN approver_id');
    await queryRunner.query('ALTER TABLE companies DROP COLUMN approved_at');
  }
}
