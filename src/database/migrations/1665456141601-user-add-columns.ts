import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class userAddColumns1665456141601 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('users', [
      new TableColumn({
        name: 'deleted_at',
        type: 'timestamptz',
        isNullable: true,
      }),
      new TableColumn({
        name: 'otp_code',
        type: 'varchar',
        length: '6',
        isNullable: true,
      }),
      new TableColumn({
        name: 'expire',
        type: 'timestamptz',
        isNullable: true,
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query('ALTER TABLE users DROP COLUMN deleted_at');
    await queryRunner.query('ALTER TABLE users DROP COLUMN otp_code');
    await queryRunner.query('ALTER TABLE users DROP COLUMN expire');
  }
}
