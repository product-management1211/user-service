import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';

class UserResponse {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;

  @ApiProperty({ example: 'abc', description: '' })
  @Expose()
  username: string;

  @ApiProperty({ example: 'abc', description: '' })
  @Expose()
  fullName: string;
}

export class FactoryResponseDto {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;

  @ApiProperty({ example: 2, description: '' })
  @Expose()
  companyId: number;

  @ApiProperty({ example: 'company 1', description: '' })
  @Expose()
  companyName: string;

  @ApiProperty({ example: 'factory 1', description: '' })
  @Expose()
  name: string;

  @ApiProperty({ example: 'ABCDEF', description: '' })
  @Expose()
  code: string;

  @ApiProperty({ example: 'Ha Noi', description: '' })
  @Expose()
  location: string;

  @ApiProperty({ example: '0955-015-1458', description: '' })
  @Expose()
  phone: string;

  @ApiProperty({ example: 'description', description: '' })
  @Expose()
  description: string;

  @ApiProperty({ example: 1, description: '' })
  @Expose()
  status: number;

  @ApiProperty({ example: '2021-07-13 09:13:15.562609+00', description: '' })
  @Expose()
  createdAt: Date;

  @ApiProperty({ example: '2021-07-13 09:13:15.562609+00', description: '' })
  @Expose()
  updatedAt: Date;

  @ApiProperty({ example: 2, description: '' })
  @Expose()
  approverId: number;

  @ApiProperty({ type: UserResponse })
  @Expose()
  @Type(() => UserResponse)
  approver: UserResponse;
}
