import { BaseDto } from '@core/dto/base.dto';
import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';

export class CreateFactoryRequestDto extends BaseDto {
  @ApiProperty({ example: '1', description: '' })
  @IsInt()
  @IsNotEmpty()
  companyId: number;

  @ApiProperty({ example: 'company 1', description: '' })
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  name: string;

  @ApiProperty({ example: 'ABCDEF', description: '' })
  @IsString()
  @MaxLength(9)
  @IsNotEmpty()
  code: string;

  @ApiPropertyOptional({ example: 'Ha Noi', description: '' })
  @IsString()
  @IsOptional()
  @MaxLength(255)
  location: string;

  @ApiPropertyOptional({ example: '0955-015-1458', description: '' })
  @IsString()
  @IsOptional()
  @MaxLength(20)
  phone: string;

  @ApiPropertyOptional({ example: 'description', description: '' })
  @IsString()
  @IsOptional()
  @MaxLength(255)
  description: string;
}
