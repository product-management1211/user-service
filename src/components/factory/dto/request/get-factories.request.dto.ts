import { PaginationQuery } from '@utils/pagination.query';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Allow, IsEnum, IsOptional } from 'class-validator';
import { Transform } from 'class-transformer';

export class GetFactoriesRequestDto extends PaginationQuery {
  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(['0', '1'])
  isGetAll: string;

  @Allow()
  @Transform(
    (value) => {
      return Number(value.value) || 1;
    },
    { toPlainOnly: true },
  )
  page?: number;
}
