import { ResponsePayload } from '@utils/response-payload';
import { CreateFactoryRequestDto } from '../dto/request/create-factory.request.dto';
import { GetFactoriesRequestDto } from '../dto/request/get-factories.request.dto';
import { SetFactoryStatusRequestDto } from '../dto/request/set-factory-status.request.dto';
import { UpdateFactoryRequestDto } from '../dto/request/update-factory.request.dto';
import { FactoryResponseDto } from '../dto/response/factory.response.dto';
import { GetFactoriesResponseDto } from '../dto/response/get-factories.response.dto';

export interface FactoryServiceInterface {
  create(
    factoryRequestDto: CreateFactoryRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>>;
  update(
    factoryDto: UpdateFactoryRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>>;
  delete(id: number): Promise<ResponsePayload<any>>;
  getDetail(id: number): Promise<ResponsePayload<FactoryResponseDto | any>>;
  getList(
    request: GetFactoriesRequestDto,
  ): Promise<ResponsePayload<GetFactoriesResponseDto | any>>;
  confirm(
    request: SetFactoryStatusRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>>;
  reject(
    request: SetFactoryStatusRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>>;
  getFactoriesByCondition(condition): Promise<any>;
}
