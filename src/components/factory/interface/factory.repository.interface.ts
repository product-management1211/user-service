import { BaseInterfaceRepository } from '@core/repository/base.interface.repository';
import { Factory } from '@entities/factory/factory.entity';
import { CreateFactoryRequestDto } from '../dto/request/create-factory.request.dto';
import { GetFactoriesRequestDto } from '../dto/request/get-factories.request.dto';

export interface FactoryRepositoryInterface
  extends BaseInterfaceRepository<Factory> {
  createEntity(factoryRequestDto: CreateFactoryRequestDto): Factory;
  getList(request: GetFactoriesRequestDto);
}
