import { Body, Controller, Inject } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { ResponsePayload } from '@utils/response-payload';
import { isEmpty } from 'lodash';
import { CreateFactoryRequestDto } from './dto/request/create-factory.request.dto';
import { GetFactoriesRequestDto } from './dto/request/get-factories.request.dto';
import { SetFactoryStatusRequestDto } from './dto/request/set-factory-status.request.dto';
import { UpdateFactoryRequestDto } from './dto/request/update-factory.request.dto';
import { FactoryResponseDto } from './dto/response/factory.response.dto';
import { GetFactoriesResponseDto } from './dto/response/get-factories.response.dto';
import { FactoryServiceInterface } from './interface/factory.service.interface';

@Controller('factories')
export class FactoryController {
  constructor(
    @Inject('FactoryServiceInterface')
    private readonly factoryService: FactoryServiceInterface,
  ) {}

  @MessagePattern('create_factory')
  public async create(
    @Body() payload: CreateFactoryRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.factoryService.create(request);
  }

  @MessagePattern('update_factory')
  public async update(
    @Body() payload: UpdateFactoryRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.factoryService.update(request);
  }

  @MessagePattern('delete_factory')
  public async delete(id: number): Promise<ResponsePayload<any>> {
    return await this.factoryService.delete(id);
  }

  @MessagePattern('detail_factory')
  public async getDetail(
    id: number,
  ): Promise<ResponsePayload<FactoryResponseDto | any>> {
    return await this.factoryService.getDetail(id);
  }

  @MessagePattern('list_factories')
  public async getList(
    @Body() payload: GetFactoriesRequestDto,
  ): Promise<ResponsePayload<GetFactoriesResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }

    return await this.factoryService.getList(request);
  }

  @MessagePattern('confirm_factory')
  public async confirm(
    @Body() payload: SetFactoryStatusRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>> {
    const { request, responseError } = payload;

    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.factoryService.confirm(request);
  }

  @MessagePattern('reject_factory')
  public async reject(
    @Body() payload: SetFactoryStatusRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>> {
    const { request, responseError } = payload;

    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.factoryService.reject(request);
  }

  @MessagePattern('get_factories_by_conditions')
  public async getFactoryByCondition(
    request,
  ): Promise<ResponsePayload<FactoryResponseDto | any>> {
    return await this.factoryService.getFactoriesByCondition(
      request.conditions,
    );
  }
}
