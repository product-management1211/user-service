import { CompanyStatusEnum } from '@components/company/company.constant';
import { CompanyRepositoryInterface } from '@components/company/interface/company.repository.interface';
import { UserRepositoryInterface } from '@components/user/interface/user.repository.interface';
import { WarehouseServiceInterface } from '@components/warehouse/interface/warehouse.service.interface';
import { Factory } from '@entities/factory/factory.entity';
import { Inject, Injectable } from '@nestjs/common';
import { ApiError } from '@utils/api.error';
import { serialize } from '@utils/helper';
import { PagingResponse } from '@utils/paging.response';
import { ResponseBuilder } from '@utils/response-builder';
import { ResponsePayload } from '@utils/response-payload';
import { plainToClass, plainToInstance } from 'class-transformer';
import { flatMap, map, uniq } from 'lodash';
import { I18nService } from 'nestjs-i18n';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import { In, Not } from 'typeorm';
import { CreateFactoryRequestDto } from './dto/request/create-factory.request.dto';
import { GetFactoriesRequestDto } from './dto/request/get-factories.request.dto';
import { SetFactoryStatusRequestDto } from './dto/request/set-factory-status.request.dto';
import { UpdateFactoryRequestDto } from './dto/request/update-factory.request.dto';
import { FactoryResponseDto } from './dto/response/factory.response.dto';
import {
  CAN_DELETE_FACTORY_STATUS,
  CAN_UPDATE_FACTORY_STATUS,
  FactoryStatusEnum,
} from './factory.constant';
import { FactoryRepositoryInterface } from './interface/factory.repository.interface';
import { FactoryServiceInterface } from './interface/factory.service.interface';

@Injectable()
export class FactoryService implements FactoryServiceInterface {
  constructor(
    @Inject('FactoryRepositoryInterface')
    private readonly factoryRepository: FactoryRepositoryInterface,

    @Inject('CompanyRepositoryInterface')
    private readonly companyRepository: CompanyRepositoryInterface,

    @Inject('WarehouseServiceInterface')
    private readonly warehouseService: WarehouseServiceInterface,

    @Inject('UserRepositoryInterface')
    private readonly userRepository: UserRepositoryInterface,
    private readonly i18n: I18nService,
  ) {}

  public async update(
    factoryDto: UpdateFactoryRequestDto,
  ): Promise<ResponsePayload<any>> {
    const factory = await this.factoryRepository.findOneByCondition({
      id: factoryDto.id,
    });

    if (!factory) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.FACTORY_NOT_FOUND'))
        .build();
    }

    if (!CAN_UPDATE_FACTORY_STATUS.includes(factory.status)) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.FACTORY_WAS_CONFIRMED'),
      ).toResponse();
    }

    const company = await this.companyRepository.findOneByCondition({
      id: factory.companyId,
    });

    if (!company) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.COMPANY_NOT_FOUND'),
      ).toResponse();
    }

    const codeCondition = [{ code: factoryDto.code, id: Not(factoryDto.id) }];

    const checkUniqueCode = await this.checkUniqueFactoryByCondition(
      codeCondition,
    );

    if (checkUniqueCode) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.CODE_ALREADY_EXISTS'))
        .build();
    }

    factory.name = factoryDto.name;
    factory.code = factoryDto.code;
    factory.description = factoryDto.description;
    factory.location = factoryDto.location;
    factory.phone = factoryDto.phone;
    factory.status = FactoryStatusEnum.CREATED;
    factory.approverId = null;
    factory.approvedAt = null;

    return await this.save(factory);
  }

  public async delete(id: number): Promise<ResponsePayload<any>> {
    try {
      const factory = await this.factoryRepository.findOneByCondition({ id });

      if (!factory) {
        return new ResponseBuilder()
          .withCode(ResponseCodeEnum.BAD_REQUEST)
          .withMessage(await this.i18n.translate('error.FACTORY_NOT_FOUND'))
          .build();
      }

      if (!CAN_DELETE_FACTORY_STATUS.includes(factory.status)) {
        return new ApiError(
          ResponseCodeEnum.BAD_REQUEST,
          await this.i18n.translate('error.FACTORY_WAS_CONFIRMED'),
        ).toResponse();
      }

      const companyWarehouse =
        await this.warehouseService.getWarehousesByConditions({
          companyId: id,
        });

      if (companyWarehouse.length > 0) {
        return new ApiError(
          ResponseCodeEnum.BAD_REQUEST,
          await this.i18n.translate('error.MUST_DELETE_WAREHOUSE'),
        ).toResponse();
      }

      await this.factoryRepository.delete(id);
      return new ResponseBuilder().withCode(ResponseCodeEnum.SUCCESS).build();
    } catch (error) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.INTERNAL_SERVER_ERROR)
        .withMessage(error?.message || error)
        .build();
    }
  }

  public async getDetail(id: number): Promise<ResponsePayload<any>> {
    const factory = await this.factoryRepository.findOneByCondition({ id });

    if (!factory) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.FACTORY_NOT_FOUND'))
        .build();
    }

    const factoryDetails = { ...factory, approver: {} };
    if (factoryDetails.approverId !== null) {
      const approver = await this.userRepository.findOneByCondition({
        id: factoryDetails.approverId,
      });
      factoryDetails.approver = approver;
    }

    const response = plainToClass(FactoryResponseDto, factoryDetails, {
      excludeExtraneousValues: true,
    });

    return new ResponseBuilder()
      .withCode(ResponseCodeEnum.SUCCESS)
      .withData(response)
      .build();
  }
  public async getList(
    request: GetFactoriesRequestDto,
  ): Promise<ResponsePayload<any>> {
    const { result, total } = await this.factoryRepository.getList(request);

    const approverIds = uniq(map(flatMap(result), 'approverId'));
    const approvers = await this.userRepository.findByCondition({
      id: In(approverIds),
    });
    const serializedApprovers = serialize(approvers);

    const factories = result.map((factory) => ({
      ...factory,
      approver: serializedApprovers[factory.approverId] || {},
    }));

    const response = plainToInstance(FactoryResponseDto, factories, {
      excludeExtraneousValues: true,
    });

    return new ResponseBuilder<PagingResponse>({
      items: response,
      meta: { total, page: request.page },
    })
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }

  public async confirm(
    request: SetFactoryStatusRequestDto,
  ): Promise<ResponsePayload<any>> {
    const { userId, id } = request;
    const factory = await this.factoryRepository.findOneByCondition({ id });
    console.log('factory', factory);

    if (!factory) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.FACTORY_NOT_FOUND'))
        .build();
    }

    const company = await this.companyRepository.findOneByCondition({
      id: factory.companyId,
      status: CompanyStatusEnum.CONFIRMED,
    });

    if (!company) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.COMPANY_NOT_CONFIRM'))
        .build();
    }

    factory.approverId = userId;
    factory.approvedAt = new Date(Date.now());
    factory.status = FactoryStatusEnum.CONFIRMED;
    const result = await this.factoryRepository.save(factory);

    const response = plainToInstance(FactoryResponseDto, result, {
      excludeExtraneousValues: true,
    });
    return new ResponseBuilder(response)
      .withCode(ResponseCodeEnum.SUCCESS)
      .withMessage(await this.i18n.translate('success.SUCCESS'))
      .build();
  }
  public async reject(
    request: SetFactoryStatusRequestDto,
  ): Promise<ResponsePayload<any>> {
    const { userId, id } = request;
    const factory = await this.factoryRepository.findOneByCondition({ id });

    if (!factory) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.FACTORY_NOT_FOUND'))
        .build();
    }

    if (factory.status === CompanyStatusEnum.CONFIRMED) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.FACTORY_WAS_CONFIRMED'),
      ).toResponse();
    }

    factory.approverId = userId;
    factory.approvedAt = new Date(Date.now());
    factory.status = FactoryStatusEnum.REJECT;
    const result = await this.factoryRepository.save(factory);

    const response = plainToInstance(FactoryResponseDto, result, {
      excludeExtraneousValues: true,
    });
    return new ResponseBuilder(response)
      .withCode(ResponseCodeEnum.SUCCESS)
      .withMessage(await this.i18n.translate('success.SUCCESS'))
      .build();
  }

  public async getFactoriesByCondition(condition: any): Promise<any> {
    const factories = await this.factoryRepository.findByCondition(condition);

    const response = plainToInstance(FactoryResponseDto, factories, {
      excludeExtraneousValues: true,
    });

    return new ResponseBuilder()
      .withCode(ResponseCodeEnum.SUCCESS)
      .withData(response)
      .build();
  }

  public async create(
    factoryRequestDto: CreateFactoryRequestDto,
  ): Promise<ResponsePayload<FactoryResponseDto | any>> {
    const company = await this.companyRepository.findOneByCondition(
      factoryRequestDto.companyId,
    );

    if (!company) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.COMPANY_NOT_FOUND'),
      ).toResponse();
    }

    const existedCode = await this.checkUniqueFactoryByCondition({
      code: factoryRequestDto.code,
    });

    if (existedCode) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.CODE_ALREADY_EXISTS'),
      ).toResponse();
    }

    const factoryEntity =
      this.factoryRepository.createEntity(factoryRequestDto);

    return await this.save(factoryEntity);
  }

  public async save(
    factoryEntity: Factory,
  ): Promise<ResponsePayload<FactoryResponseDto> | any> {
    try {
      const result = await this.factoryRepository.save(factoryEntity);

      const response = plainToClass(FactoryResponseDto, result, {
        excludeExtraneousValues: true,
      });

      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.SUCCESS)
        .withData(response)
        .build();
    } catch (error) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.INTERNAL_SERVER_ERROR)
        .withMessage(error?.message || error)
        .build();
    }
  }

  private async checkUniqueFactoryByCondition(
    condition: any,
  ): Promise<boolean> {
    const result = await this.factoryRepository.findByCondition(condition);
    return result.length > 0;
  }
}
