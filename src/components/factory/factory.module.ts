import { UserModule } from '@components/user/user.module';
import { UserService } from '@components/user/user.service';
import { WarehouseModule } from '@components/warehouse/warehouse.module';
import { WarehouseService } from '@components/warehouse/warehouse.service';
import { Company } from '@entities/company/company.entity';
import { Factory } from '@entities/factory/factory.entity';
import { User } from '@entities/user/user.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompanyRepository } from 'src/repository/company.repository';
import { FactoryRepository } from 'src/repository/factory.repository';
import { UserRepository } from 'src/repository/user.repository';
import { FactoryController } from './factory.controller';
import { FactoryService } from './factory.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Factory, Company, User]),
    WarehouseModule,
  ],
  providers: [
    {
      provide: 'FactoryRepositoryInterface',
      useClass: FactoryRepository,
    },
    {
      provide: 'FactoryServiceInterface',
      useClass: FactoryService,
    },
    {
      provide: 'CompanyRepositoryInterface',
      useClass: CompanyRepository,
    },
    {
      provide: 'WarehouseServiceInterface',
      useClass: WarehouseService,
    },
    {
      provide: 'UserRepositoryInterface',
      useClass: UserRepository,
    },
  ],
  controllers: [FactoryController],
})
export class FactoryModule {}
