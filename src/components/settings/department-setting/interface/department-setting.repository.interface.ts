import { BaseInterfaceRepository } from '@core/repository/base.interface.repository';
import { DepartmentSettings } from '@entities/department-setting/department-setting.entity';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface DepartmentSettingRepositoryInterface
  extends BaseInterfaceRepository<DepartmentSettings> {}
