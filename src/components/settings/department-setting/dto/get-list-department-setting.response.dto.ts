import { Expose } from 'class-transformer';

export class GetListDepartmentSettingResponseDto {
  @Expose()
  id: number;

  @Expose()
  name: string;
}
