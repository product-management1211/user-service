import { Injectable } from '@nestjs/common';
import { ResponsePayload } from '@utils/response-payload';
import { I18nService } from 'nestjs-i18n';
import { SendMailRequestDto } from './dto/request/send-mail.request.dto';
import { MailServiceInterface } from './interface/mail.service.interface';
import { MailerService } from '@nestjs-modules/mailer';
import { ResponseBuilder } from '@utils/response-builder';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import { ApiError } from '@utils/api.error';

@Injectable()
export class MailService implements MailServiceInterface {
  constructor(
    private readonly i18n: I18nService,
    private mailerService: MailerService,
  ) {}

  public async sendMail(
    request: SendMailRequestDto,
  ): Promise<ResponsePayload<any>> {
    try {
      const { email, body } = request;
      await this.mailerService.sendMail({
        to: email,
        subject: body.subject,
        template: body.template,
        context: body.context,
      });
      return new ResponseBuilder().withCode(ResponseCodeEnum.SUCCESS).build();
    } catch (error) {
      console.log(error);
      return new ApiError(
        ResponseCodeEnum.NOT_FOUND,
        await this.i18n.translate('error.NOT_FOUND'),
      ).toResponse();
    }
  }
}
