import { Controller, Inject, Req } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { ResponseBuilder } from '@utils/response-builder';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import { MailServiceInterface } from './interface/mail.service.interface';

@Controller('mails')
export class MailController {
  constructor(
    @Inject('MailServiceInterface')
    private readonly mailService: MailServiceInterface,
  ) {}

  @MessagePattern('ping')
  public async get(@Req() request): Promise<any> {
    return new ResponseBuilder('PONG')
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }
}
