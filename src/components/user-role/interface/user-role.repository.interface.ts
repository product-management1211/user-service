import { BaseInterfaceRepository } from '@core/repository/base.interface.repository';
import { UserRole } from '@entities/user-role/user-role.entity';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface UserRoleRepositoryInterface
  extends BaseInterfaceRepository<UserRole> {}
