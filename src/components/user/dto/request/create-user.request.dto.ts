import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { AbstractUserRequestDto } from './abstract-user.request.dto';

export class CreateUserRequestDto extends AbstractUserRequestDto {
  @ApiProperty({ example: '123456789', description: 'password' })
  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(255)
  password: string;
}
