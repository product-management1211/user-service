import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';
import { AbstractUserRequestDto } from './abstract-user.request.dto';

export class UpdateUserRequestDto extends AbstractUserRequestDto {
  @ApiProperty({ example: '1', description: 'id' })
  @IsNotEmpty()
  @IsInt()
  id: number;
}
