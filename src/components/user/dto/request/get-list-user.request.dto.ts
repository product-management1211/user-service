import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsOptional,
  IsString,
  IsEnum,
  IsNotEmpty,
  IsArray,
} from 'class-validator';
import { Type } from 'class-transformer';
import { PaginationQuery } from '@utils/pagination.query';


export class GetListUserRequestDto extends PaginationQuery {
  @ApiPropertyOptional({ example: 'user', description: '' })
  @IsOptional()
  @IsString()
  keyword?: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsEnum(['0', '1'])
  isGetAll: string;

  @IsOptional()
  @IsString()
  username: string;
}
