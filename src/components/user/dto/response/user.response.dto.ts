import { WarehouseResponseDto } from '@components/warehouse/dto/response/warehouse.response.dto';
import { ApiProperty } from '@nestjs/swagger';
import { Expose, Type } from 'class-transformer';
import { FactoryResponseDto } from 'src/components/factory/dto/response/factory.response.dto';
import { GetListDepartmentSettingResponseDto } from 'src/components/settings/department-setting/dto/get-list-department-setting.response.dto';
import { GetListUserRoleSettingResponseDto } from 'src/components/user-role-setting/dto/response/get-list-user-role-setting.response.dto';

export class UserResponseDto {
  @ApiProperty({ example: 1, description: '' })
  @Expose()
  id: number;

  @ApiProperty({ example: 'abc@gmail.com', description: '' })
  @Expose()
  email: string;

  @ApiProperty({ example: 'abc', description: '' })
  @Expose()
  username: string;

  @ApiProperty({ example: 'abc', description: '' })
  @Expose()
  fullName: string;

  @ApiProperty({ example: 1, description: '' })
  @Expose()
  companyId: string;

  @ApiProperty({ example: '2021-07-01', description: '' })
  @Expose()
  dateOfBirth: string;

  @ApiProperty({ example: 'abc', description: '' })
  @Expose()
  code: string;

  @ApiProperty({ example: '0987-1254-125', description: '' })
  @Expose()
  phone: string;

  @ApiProperty({ example: 1, description: '' })
  @Expose()
  status: number;

  @Expose()
  createdAt: string;

  @Expose()
  updatedAt: string;

  @ApiProperty({
    type: WarehouseResponseDto,
    example: [{ id: 1, name: 'warehouse 1' }],
    description: '',
  })
  @Expose()
  @Type(() => WarehouseResponseDto)
  userWarehouses: WarehouseResponseDto[];

  @ApiProperty({
    type: GetListUserRoleSettingResponseDto,
    example: [{ id: 1, name: 'role 1' }],
    description: '',
  })
  @Expose()
  @Type(() => GetListUserRoleSettingResponseDto)
  userRoleSettings: GetListUserRoleSettingResponseDto[];

  @ApiProperty({
    type: GetListDepartmentSettingResponseDto,
    example: [{ id: 1, name: 'department 1' }],
    description: '',
  })
  @Expose()
  @Type(() => GetListDepartmentSettingResponseDto)
  departmentSettings: GetListDepartmentSettingResponseDto[];

  @ApiProperty({
    type: FactoryResponseDto,
    example: [{ id: 1, name: 'factory 1' }],
    description: '',
  })
  @Expose()
  @Type(() => FactoryResponseDto)
  factories: FactoryResponseDto[];
}
