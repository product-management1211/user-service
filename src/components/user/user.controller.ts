import { Controller, Inject } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { SuccessResponse } from '@utils/success.response.dto';
import { isEmpty } from 'lodash';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import isEmptyObject from 'src/utils/helper';
import { ResponseBuilder } from 'src/utils/response-builder';
import { ResponsePayload } from 'src/utils/response-payload';
import { CreateUserRequestDto } from './dto/request/create-user.request.dto';
import { GetListUserRequestDto } from './dto/request/get-list-user.request.dto';
import { UpdateUserRequestDto } from './dto/request/update-user.request.dto';
import { GetListUserResponseDto } from './dto/response/get-list-user.response.dto';
import { UserResponseDto } from './dto/response/user.response.dto';
import { UserServiceInterface } from './interface/user.service.interface';

@Controller('users')
export class UserController {
  constructor(
    @Inject('UserServiceInterface')
    private readonly userService: UserServiceInterface,
  ) {}

  @MessagePattern('ping')
  public async get(): Promise<any> {
    return new ResponseBuilder('Ping succeed')
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }

  @MessagePattern('create')
  public async register(
    payload: CreateUserRequestDto,
  ): Promise<ResponsePayload<UserResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return this.userService.create(request);
  }

  @MessagePattern('delete')
  public async delete(
    id: number,
  ): Promise<ResponsePayload<SuccessResponse | any>> {
    return await this.userService.remove(id);
  }

  @MessagePattern('list')
  public async getList(
    payload: GetListUserRequestDto,
  ): Promise<ResponsePayload<GetListUserResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.userService.getList(request);
  }

  @MessagePattern('list_sync')
  public async getListSync(): Promise<
    ResponsePayload<GetListUserResponseDto | any>
  > {
    return await this.userService.getListSync();
  }

  @MessagePattern('detail')
  public async detail(
    id: number,
  ): Promise<ResponsePayload<UserResponseDto | any>> {
    return await this.userService.getDetail(id);
  }

  @MessagePattern('update')
  public async update(
    payload: UpdateUserRequestDto,
  ): Promise<ResponsePayload<UserResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.userService.update(request);
  }
}
