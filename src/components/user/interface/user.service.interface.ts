import { SuccessResponse } from '@utils/success.response.dto';
import { ResponsePayload } from 'src/utils/response-payload';
import { CreateUserRequestDto } from '../dto/request/create-user.request.dto';
import { GetListUserRequestDto } from '../dto/request/get-list-user.request.dto';
import { UpdateUserRequestDto } from '../dto/request/update-user.request.dto';
import { GetListUserResponseDto } from '../dto/response/get-list-user.response.dto';
import { UserResponseDto } from '../dto/response/user.response.dto';

export interface UserServiceInterface {
  create(
    request: CreateUserRequestDto,
  ): Promise<ResponsePayload<UserResponseDto | any>>;
  remove(id: number): Promise<ResponsePayload<SuccessResponse | any>>;
  getList(
    request: GetListUserRequestDto,
  ): Promise<ResponsePayload<GetListUserResponseDto | any>>;
  getListSync(): Promise<ResponsePayload<GetListUserResponseDto | any>>;
  getDetail(id: number): Promise<ResponsePayload<UserResponseDto | any>>;
  update(
    request: UpdateUserRequestDto,
  ): Promise<ResponsePayload<UserResponseDto | any>>;
}
