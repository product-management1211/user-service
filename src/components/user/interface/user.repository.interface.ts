import { UserDepartment } from '@entities/user-department/user-department.entity';
import { UserFactory } from '@entities/user-factory/user-factory.entity';
import { UserRole } from '@entities/user-role/user-role.entity';
import { UserWarehouse } from '@entities/user-warehouse/user-warehouse.entity';
import { BaseInterfaceRepository } from 'src/core/repository/base.interface.repository';
import { User } from 'src/entities/user/user.entity';

export interface UserRepositoryInterface extends BaseInterfaceRepository<User> {
  checkUniqueUser(condition: any): Promise<any>;
  createUserFactoryEntity(userId: number, factoryId: number): UserFactory;
  createUserDepartmentEntity(
    userId: number,
    departmentId: number,
  ): UserDepartment;
  createUserRoleEntity(
    userId: number,
    roleId: number,
    departmentId: number,
  ): UserRole;
  createUserWarehouseEntity(userId: number, warehouseId: number): UserWarehouse;
  getUsers(request: any, warehouseIds?: number[]): Promise<any>;
  validateUser(username: string, password: string): Promise<any>;
  getDetail(id: number): Promise<any>;
  isSuperAdmin(code: string): boolean;
}
