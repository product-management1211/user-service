import { CompanyRepositoryInterface } from '@components/company/interface/company.repository.interface';
import { UserRoleSettingRepositoryInterface } from '@components/settings/user-role-setting/interface/user-role-setting.repository.interface';
import { Inject, Injectable } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import { User } from 'src/entities/user/user.entity';
import { ApiError } from 'src/utils/api.error';
import { ResponsePayload } from 'src/utils/response-payload';
import { CreateUserRequestDto } from './dto/request/create-user.request.dto';
import { UserResponseDto } from './dto/response/user.response.dto';
import { UserRepositoryInterface } from './interface/user.repository.interface';
import { UserServiceInterface } from './interface/user.service.interface';
import { uniq, isEmpty, map, flatMap } from 'lodash';
import { DataSource, In, Not } from 'typeorm';
import { DepartmentSettingRepositoryInterface } from '@components/settings/department-setting/interface/department-setting.repository.interface';
import { FactoryRepositoryInterface } from '@components/factory/interface/factory.repository.interface';
import { plainToInstance } from 'class-transformer';
import { ResponseBuilder } from '@utils/response-builder';
import { WarehouseService } from '@components/warehouse/warehouse.service';
import { SuccessResponse } from '@utils/success.response.dto';
import { DEFAULT_DATA } from 'src/constants/common';
import { UserFactory } from '@entities/user-factory/user-factory.entity';
import { UserDepartment } from '@entities/user-department/user-department.entity';
import { UserRole } from '@entities/user-role/user-role.entity';
import { UserWarehouse } from '@entities/user-warehouse/user-warehouse.entity';
import { GetListUserRequestDto } from './dto/request/get-list-user.request.dto';
import { GetListUserResponseDto } from './dto/response/get-list-user.response.dto';
import { PagingResponse } from '@utils/paging.response';
import { UserSyncResponseDto } from './dto/response/user-sync.response.dto';
import { UpdateUserRequestDto } from './dto/request/update-user.request.dto';

@Injectable()
export class UserService implements UserServiceInterface {
  constructor(
    @Inject('UserRepositoryInterface')
    private readonly userRepository: UserRepositoryInterface,

    @Inject('CompanyRepositoryInterface')
    private readonly companyRepository: CompanyRepositoryInterface,

    @Inject('UserRoleSettingRepositoryInterface')
    private readonly userRoleSettingRepository: UserRoleSettingRepositoryInterface,

    @Inject('DepartmentSettingRepositoryInterface')
    private readonly departmentSettingRepository: DepartmentSettingRepositoryInterface,

    @Inject('FactoryRepositoryInterface')
    private readonly factoryRepository: FactoryRepositoryInterface,

    @Inject('WarehouseServiceInterface')
    private readonly warehouseService: WarehouseService,

    private readonly i18n: I18nService,

    private dataSource: DataSource,
  ) {}
  async update(request: UpdateUserRequestDto): Promise<ResponsePayload<any>> {
    const {
      id,
      fullName,
      dateOfBirth,
      phone,
      email,
      companyId,
      username,
      code,
    } = request;

    const user = await this.userRepository.findOneByCondition({ id });
    if (isEmpty(user)) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.USER_NOT_FOUND'),
      ).toResponse();
    }

    try {
      await this.checkExistingUserFields({ username, code, email }, id);
    } catch (error) {
      return error._errorData
        ? new ApiError(
            ResponseCodeEnum.BAD_REQUEST,
            error._message,
            error._errorData,
          ).toResponseWithData()
        : new ApiError(
            ResponseCodeEnum.BAD_REQUEST,
            error._message,
          ).toResponse();
    }

    user.fullName = fullName || user.fullName;
    user.dateOfBirth = dateOfBirth || user.dateOfBirth;
    user.phone = phone || user.phone;
    user.email = email;
    user.companyId = companyId || user.companyId;
    return await this.save(user, request);
  }

  async getDetail(id: number): Promise<ResponsePayload<any>> {
    const user = await this.userRepository.getDetail(id);
    if (!user) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.USER_NOT_FOUND'),
      ).toResponse();
    }

    let normalizedWarehouses = [];
    if (this.userRepository.isSuperAdmin(user.code)) {
      normalizedWarehouses =
        await this.warehouseService.getWarehousesByConditions({});

      user.userWarehouses = normalizedWarehouses.map((warehouse) => ({
        ...warehouse,
      }));
    } else {
      const warehouseIds = uniq(map(user, 'userWarehouses'));
      const warehouses = await this.warehouseService.getListByIDs(warehouseIds);
      if (warehouses?.length > 0) {
        warehouses.forEach((warehouse) => {
          normalizedWarehouses[warehouse.id] = warehouse;
        });
        user.userWarehouses = user.userWarehouses.map((warehouse) => ({
          ...normalizedWarehouses[warehouse.id],
        }));
      }
    }
    const dataReturn = plainToInstance(UserResponseDto, user, {
      excludeExtraneousValues: true,
    });
    return new ResponseBuilder(dataReturn)
      .withCode(ResponseCodeEnum.SUCCESS)
      .withData(user)
      .build();
  }

  async getListSync(): Promise<ResponsePayload<any>> {
    const data = await this.userRepository.findWithRelations({
      relations: ['userWarehouses'],
      select: ['password', 'id', 'username', 'fullName'],
    });
    const dataReturn = plainToInstance(UserSyncResponseDto, data, {
      excludeExtraneousValues: true,
    });
    return new ResponseBuilder(dataReturn)
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }

  /*---------------------Main functions-----------------------*/
  /**
   * Get user list
   * @param request
   * @returns
   */
  public async getList(
    payload: GetListUserRequestDto,
  ): Promise<ResponsePayload<GetListUserResponseDto | any>> {
    const warehouseNameConditions = payload.filter?.find(
      (condition) => condition.column === 'warehouseName',
    );
    console.log(warehouseNameConditions);
    let warehouseIds = [];

    if (!isEmpty(warehouseNameConditions)) {
      warehouseIds = await this.warehouseService.getWarehousesByName(
        warehouseNameConditions,
        true,
      );
      if (isEmpty(warehouseIds)) {
        return new ResponseBuilder<PagingResponse>({
          items: [],
          meta: { total: 0, page: payload.page },
        })
          .withCode(ResponseCodeEnum.SUCCESS)
          .build();
      }
    }

    const { data, count } = await this.userRepository.getUsers(
      payload,
      warehouseIds,
    );
    let result;
    if (!isEmpty(data) && count > 0) {
      const warehouseIds = uniq(map(flatMap(data, 'userWarehouses'), 'id'));
      console.log('warehouseIds', warehouseIds);
      const warehouses = await this.warehouseService.getListByIDs(warehouseIds);
      const normalizedWarehouse = {};

      warehouses.forEach((warehouse) => {
        normalizedWarehouse[warehouse.id] = warehouse;
      });

      const users = data.map((user) => ({
        ...user,
        userWarehouses: user.userWarehouses.map((warehouse) => ({
          ...normalizedWarehouse[warehouse.id],
        })),
      }));

      result = plainToInstance(UserResponseDto, users, {
        excludeExtraneousValues: true,
      });
    }

    return new ResponseBuilder<PagingResponse>({
      items: !isEmpty(result) ? result : [],
      meta: { total: count, page: payload.page },
    })
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }

  public async remove(
    id: number,
  ): Promise<ResponsePayload<SuccessResponse | any>> {
    const user = await this.userRepository.findOneByCondition({
      id,
    });
    if (isEmpty(user)) {
      return new ApiError(
        ResponseCodeEnum.NOT_FOUND,
        await this.i18n.translate('error.USER_NOT_FOUND'),
      ).toResponse();
    }

    const isDefaultUser =
      DEFAULT_DATA.USERS.filter((defaultUser) => defaultUser.code === user.code)
        .length > 0
        ? true
        : false;

    if (isDefaultUser) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.CAN_NOT_DELETE'),
      ).toResponse();
    }
    const queryRunner = await this.dataSource.createQueryRunner();
    queryRunner.startTransaction();
    try {
      await queryRunner.manager.delete(UserFactory, { userId: id });
      await queryRunner.manager.delete(UserDepartment, { userId: id });
      await queryRunner.manager.delete(UserRole, { userId: id });
      await queryRunner.manager.delete(UserWarehouse, { userId: id });
      await queryRunner.manager.delete(User, { id: id });
      await queryRunner.commitTransaction();
      return new ResponseBuilder().withCode(ResponseCodeEnum.SUCCESS).build();
    } catch (error) {
      console.log(error);
      await queryRunner.rollbackTransaction();
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.INTERNAL_SERVER_ERROR)
        .withMessage(error?.message || error)
        .build();
    } finally {
      queryRunner.release();
    }
  }

  public async create(
    request: CreateUserRequestDto,
  ): Promise<ResponsePayload<any>> {
    try {
      await this.checkExistingUserFields(request);
      await this.checkUserFieldsNotFound(request);
    } catch (error) {
      return error._errorData
        ? new ApiError(
            ResponseCodeEnum.BAD_REQUEST,
            error._message,
            error._errorData,
          ).toResponseWithData()
        : new ApiError(
            ResponseCodeEnum.BAD_REQUEST,
            error._message,
          ).toResponse();
    }

    const userEntity = this.userRepository.createEntity(request);
    return await this.save(userEntity, request);
  }

  private async save(
    userEntity: User,
    payload: any,
  ): Promise<ResponsePayload<UserResponseDto | any>> {
    const { userRoleSettings, departmentSettings, factories, userWarehouses } =
      payload;
    const isUpdate = userEntity.id !== null;

    const queryRunner = this.dataSource.createQueryRunner();
    queryRunner.startTransaction();

    try {
      const user = await queryRunner.manager.save(userEntity);

      if (!isEmpty(factories)) {
        const userFactoryEntities: any = factories.map((factory) =>
          this.userRepository.createUserFactoryEntity(user.id, factory.id),
        );

        if (isUpdate) {
          await queryRunner.manager.delete(UserFactory, {
            userId: user.id,
          });
        }
        user.factories = await queryRunner.manager.save(userFactoryEntities);
      }

      if (!isEmpty(departmentSettings)) {
        const userDepartmentEntities: any = departmentSettings.map(
          (department) =>
            this.userRepository.createUserDepartmentEntity(
              user.id,
              department.id,
            ),
        );

        if (isUpdate) {
          await queryRunner.manager.delete(UserDepartment, {
            userId: user.id,
          });
        }
        user.departmentSettings = await queryRunner.manager.save(
          userDepartmentEntities,
        );
      }

      if (!isEmpty(userRoleSettings)) {
        const userRoleEntities: any = userRoleSettings.map((department) =>
          this.userRepository.createUserRoleEntity(
            user.id,
            userRoleSettings[0].id,
            department.id,
          ),
        );

        if (isUpdate) {
          await queryRunner.manager.delete(UserRole, {
            userId: user.id,
          });
        }
        user.userRoleSettings = await queryRunner.manager.save(
          userRoleEntities,
        );
      }

      if (!isEmpty(userWarehouses)) {
        const userWarehouseEntities: any = userWarehouses.map((warehouse) =>
          this.userRepository.createUserWarehouseEntity(user.id, warehouse.id),
        );

        await queryRunner.manager.delete(UserWarehouse, {
          userId: user.id,
        });
        user.userWarehouses = await queryRunner.manager.save(
          userWarehouseEntities,
        );
      }
      await queryRunner.commitTransaction();

      const response = plainToInstance(UserResponseDto, user, {
        excludeExtraneousValues: true,
      });

      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.SUCCESS)
        .withData(response)
        .build();
    } catch (error) {
      console.log(error);
      await queryRunner.rollbackTransaction();
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.INTERNAL_SERVER_ERROR)
        .withMessage(error)
        .build();
    } finally {
      console.log('release queryRunner');

      await queryRunner.release();
    }
  }

  /*---------------------Helper functions-----------------------*/
  private async checkUserFieldsNotFound(request: CreateUserRequestDto) {
    const {
      companyId,
      userRoleSettings,
      departmentSettings,
      factories,
      userWarehouses,
    } = request;

    const company = await this.companyRepository.findOneByCondition({
      id: companyId,
    });
    if (isEmpty(company)) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.COMPANY_NOT_FOUND'),
      );
    }

    if (!isEmpty(userRoleSettings)) {
      const roleIds = uniq(userRoleSettings.map((userRole) => userRole.id));
      const existingRoles =
        await this.userRoleSettingRepository.findByCondition({
          id: In(roleIds),
        });
      await this.validateObjects(roleIds, existingRoles, 'USER_ROLE');
    }

    if (!isEmpty(departmentSettings)) {
      const departmentIds = uniq(departmentSettings.map((item) => item.id));
      const existingDepartments =
        await this.departmentSettingRepository.findByCondition({
          id: In(departmentIds),
        });
      await this.validateObjects(
        departmentIds,
        existingDepartments,
        'DEPARTMENT',
      );
    }

    if (!isEmpty(factories)) {
      const factoryIds = uniq(factories.map((factory) => factory.id));
      const existingFactories = await this.factoryRepository.findByCondition({
        id: In(factoryIds),
      });

      await this.validateObjects(factoryIds, existingFactories, 'FACTORY');
    }

    if (!isEmpty(userWarehouses)) {
      //TODO: Validate warehouse
      const warehouseIds = uniq(
        userWarehouses.map((warehouse) => warehouse.id),
      );
      const existingWarehouses = await this.warehouseService.getListByIDs(
        warehouseIds,
      );
      await this.validateObjects(warehouseIds, existingWarehouses, 'WAREHOUSE');
    }
  }

  private async checkExistingUserFields(request, id?: number) {
    const { code, username, email } = request;
    console.log('username', username);

    const existedUsername = id
      ? await this.checkUniqueUser({ username, id: Not(id) })
      : await this.checkUniqueUser({ username });
    if (existedUsername) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.USERNAME_ALREADY_EXISTS'),
      );
    }

    const existedCode = id
      ? await this.checkUniqueUser({ code, id: Not(id) })
      : await this.checkUniqueUser({ code });
    if (existedCode) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.CODE_ALREADY_EXISTS'),
      );
    }

    const existedEmail = id
      ? await this.checkUniqueUser({ email, id: Not(id) })
      : await this.checkUniqueUser({ email });
    if (existedEmail) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.EMAIL_ALREADY_EXISTS'),
      );
    }
  }

  private async validateObjects(
    ids: number[],
    objects: any[],
    errorObjectString: string,
  ) {
    if (isEmpty(objects)) {
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate(`error.${errorObjectString}_NOT_FOUND`),
      );
    }

    if (objects.length != ids.length) {
      const existObjectIds = objects.map((obj) => obj.id);
      const nonexistentObjectIds = ids.filter(
        (id) => !existObjectIds.includes(id),
      );
      throw new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate(`error.${errorObjectString}_NOT_FOUND`),
        {
          object: errorObjectString,
          nonexistentObjectIds: [...nonexistentObjectIds],
        },
      );
    }
  }

  private async checkUniqueUser(condition: any): Promise<boolean> {
    const user = await this.userRepository.checkUniqueUser(condition);
    console.log('user', user);

    return user.length > 0;
  }
}
