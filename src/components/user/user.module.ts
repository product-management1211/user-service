import { WarehouseModule } from '@components/warehouse/warehouse.module';
import { WarehouseService } from '@components/warehouse/warehouse.service';
import { Company } from '@entities/company/company.entity';
import { DepartmentSettings } from '@entities/department-setting/department-setting.entity';
import { Factory } from '@entities/factory/factory.entity';
import { UserRoleSetting } from '@entities/user-role-setting/user-role-setting.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/entities/user/user.entity';
import { CompanyRepository } from 'src/repository/company.repository';
import { DepartmentSettingRepository } from 'src/repository/department-setting.repository';
import { FactoryRepository } from 'src/repository/factory.repository';
import { UserRoleSettingRepository } from 'src/repository/user-role-setting.repository';
import { UserRepository } from 'src/repository/user.repository';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      Company,
      UserRoleSetting,
      DepartmentSettings,
      Factory,
    ]),
    WarehouseModule,
  ],
  controllers: [UserController],
  providers: [
    {
      provide: 'UserRepositoryInterface',
      useClass: UserRepository,
    },
    {
      provide: 'UserServiceInterface',
      useClass: UserService,
    },
    {
      provide: 'CompanyRepositoryInterface',
      useClass: CompanyRepository,
    },
    {
      provide: 'UserRoleSettingRepositoryInterface',
      useClass: UserRoleSettingRepository,
    },
    {
      provide: 'DepartmentSettingRepositoryInterface',
      useClass: DepartmentSettingRepository,
    },
    {
      provide: 'FactoryRepositoryInterface',
      useClass: FactoryRepository,
    },
    {
      provide: 'WarehouseServiceInterface',
      useClass: WarehouseService,
    },
  ],
  exports: [
    {
      provide: 'UserServiceInterface',
      useClass: UserService,
    },
    {
      provide: 'UserRepositoryInterface',
      useClass: UserRepository,
    },
    {
      provide: 'CompanyRepositoryInterface',
      useClass: CompanyRepository,
    },
    {
      provide: 'UserRoleSettingRepositoryInterface',
      useClass: UserRoleSettingRepository,
    },
    {
      provide: 'DepartmentSettingRepositoryInterface',
      useClass: DepartmentSettingRepository,
    },
    {
      provide: 'FactoryRepositoryInterface',
      useClass: FactoryRepository,
    },
  ],
})
export class UserModule {}
