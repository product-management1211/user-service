import { BaseDto } from "@core/dto/base.dto";
import { ApiProperty } from "@nestjs/swagger";
import { RememberPassword } from "@utils/common";
import { IsEnum, IsNotEmpty, IsOptional, IsString, MaxLength } from "class-validator";

export class LoginRequestDto extends BaseDto {
    @ApiProperty()
    @IsString()
    @MaxLength(255)
    @IsNotEmpty()
    username: string;

    @ApiProperty()
    @IsString()
    @MaxLength(255)
    @IsNotEmpty()
    password: string;

    @ApiProperty()
    @IsOptional()
    @IsEnum(RememberPassword)
    rememberPassword: number;
}
