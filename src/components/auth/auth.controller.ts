import { Body, Controller, Inject } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { ResponsePayload } from '@utils/response-payload';
import { isEmpty } from 'lodash';
import { CheckOtpRequestDto } from './dto/request/check-otp.request.dto';
import { GenerateOtpRequestDto } from './dto/request/genarate-otp.request.dto';
import { LoginRequestDto } from './dto/request/login.request.dto';
import { ResetPasswordRequestDto } from './dto/request/reset-password.request.dto';
import { ForgotPasswordResponseDto } from './dto/response/forgot-password.response.dto';
import { AuthServiceInterface } from './interface/auth.service.interface';

@Controller('auth')
export class AuthController {
  constructor(
    @Inject('AuthServiceInterface')
    private readonly authService: AuthServiceInterface,
  ) {}

  @MessagePattern('login')
  public async login(@Body() payload: LoginRequestDto): Promise<any> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }

    return await this.authService.login(request);
  }

  @MessagePattern('validate_token')
  public async validateToken(payload: any): Promise<any> {
    return await this.authService.validateToken(payload);
  }

  @MessagePattern('refresh_token')
  public async refreshToken(payload: any): Promise<any> {
    return await this.authService.refreshToken(payload);
  }

  @MessagePattern('generate_otp')
  public async generateOtp(
    payload: GenerateOtpRequestDto,
  ): Promise<ResponsePayload<ForgotPasswordResponseDto | any>> {
    console.log(payload);
    const { request, responseError } = payload;

    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.authService.generateOpt(request);
  }

  @MessagePattern('check_opt')
  public async checkOtp(
    payload: CheckOtpRequestDto,
  ): Promise<ResponsePayload<ForgotPasswordResponseDto | any>> {
    const { request, responseError } = payload;

    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.authService.checkOtp(request);
  }

  @MessagePattern('reset_password')
  public async forgotPasswordResetPassword(
    payload: ResetPasswordRequestDto,
  ): Promise<ResponsePayload<any>> {
    const { request, responseError } = payload;

    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }

    return await this.authService.resetPassword(request);
  }
}
