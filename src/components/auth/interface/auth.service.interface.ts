import { ResponsePayload } from '@utils/response-payload';
import { CheckOtpRequestDto } from '../dto/request/check-otp.request.dto';
import { GenerateOtpRequestDto } from '../dto/request/genarate-otp.request.dto';
import { LoginRequestDto } from '../dto/request/login.request.dto';
import { ResetPasswordRequestDto } from '../dto/request/reset-password.request.dto';
import { ForgotPasswordResponseDto } from '../dto/response/forgot-password.response.dto';

export interface AuthServiceInterface {
  login(payload: LoginRequestDto): Promise<ResponsePayload<any>>;
  _createToken(id: number, code: string, username: string): any;
  _createRefreshToken(id: number, rememberPassword?: number): any;
  validateToken(payload: any): Promise<any>;
  refreshToken(payload: any): Promise<any>;
  generateOpt(
    request: GenerateOtpRequestDto,
  ): Promise<ResponsePayload<ForgotPasswordResponseDto | any>>;
  checkOtp(
    request: CheckOtpRequestDto,
  ): Promise<ResponsePayload<ForgotPasswordResponseDto> | any>;
  resetPassword(
    request: ResetPasswordRequestDto,
  ): Promise<ResponsePayload<any>>;
}
