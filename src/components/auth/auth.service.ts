import { UserRepositoryInterface } from '@components/user/interface/user.repository.interface';
import { UserServiceInterface } from '@components/user/interface/user.service.interface';
import { Inject } from '@nestjs/common';
import { ResponsePayload } from '@utils/response-payload';
import { I18nService } from 'nestjs-i18n';
import { LoginRequestDto } from './dto/request/login.request.dto';
import { AuthServiceInterface } from './interface/auth.service.interface';
import { ApiError } from 'src/utils/api.error';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from 'src/constants/common';
import { RememberPassword } from '@utils/common';
import { plainToInstance } from 'class-transformer';
import { LoginResponseDto } from './dto/response/login.response.dto';
import { ResponseBuilder } from '@utils/response-builder';
import { isEmpty } from 'lodash';
import { CheckOtpRequestDto } from './dto/request/check-otp.request.dto';
import { GenerateOtpRequestDto } from './dto/request/genarate-otp.request.dto';
import { ResetPasswordRequestDto } from './dto/request/reset-password.request.dto';
import { ConfigService } from '@config/config.service';
import { MailService } from '@components/mail/mail.service';
import { UserService } from '@components/user/user.service';
import { SendMailRequestDto } from '@components/mail/dto/request/send-mail.request.dto';
import * as bcrypt from 'bcrypt';
export class AuthService implements AuthServiceInterface {
  constructor(
    @Inject('UserRepositoryInterface')
    private readonly userRepository: UserRepositoryInterface,

    @Inject('UserServiceInterface')
    private readonly userService: UserService,

    @Inject('MailServiceInterface')
    private readonly mailService: MailService,

    private readonly i18n: I18nService,

    private readonly jwtService: JwtService,
  ) {}
  public async generateOpt(
    request: GenerateOtpRequestDto,
  ): Promise<ResponsePayload<any>> {
    const { email } = request;
    const user = await this.userRepository.findOneByCondition({ email });

    if (!user) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.USER_NOT_FOUND'),
      ).toResponse();
    }

    const otpCode = this.randomNumber(
      new ConfigService().get('otpMinNumber'),
      new ConfigService().get('otpMaxNumber'),
    );
    const timeout = new ConfigService().get('otpTimeout');
    const expire = new Date(
      new Date().setTime(new Date().getTime() + parseInt(timeout) * 1000),
    );

    user.otpCode = otpCode.toString();
    user.expire = expire;

    await this.userRepository.update(user);

    const body = {
      subject: 'OTP CODE',
      template: './generate-otp',
      context: {
        code: otpCode,
        timeout: timeout / 60,
      },
    };

    const requestMail = new SendMailRequestDto();
    requestMail.email = email;
    requestMail.body = body;

    const sendMail = await this.mailService.sendMail(requestMail);

    if (sendMail.statusCode !== ResponseCodeEnum.SUCCESS) {
      return new ApiError(
        ResponseCodeEnum.INTERNAL_SERVER_ERROR,
        await this.i18n.translate('error.SEND_MAIL_FAIL'),
      ).toResponse();
    }

    const response = {
      email: email,
    };

    return new ResponseBuilder()
      .withCode(ResponseCodeEnum.SUCCESS)
      .withData(response)
      .build();
  }

  private randomNumber(min: number, max: number) {
    return Math.floor(Math.random() * (max - min)) + Math.floor(min);
  }

  public async checkOtp(request: CheckOtpRequestDto): Promise<any> {
    const { email, code } = request;
    const user = await this.userRepository.findOneByCondition({ email });

    if (!user) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.USER_NOT_FOUND'),
      ).toResponse();
    }

    if (code !== user.otpCode) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.OTP_CODE_NOT_CORRECT'),
      ).toResponse();
    }
    if (user.expire < new Date(new Date().getTime())) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.OTP_CODE_EXPIRED'),
      ).toResponse();
    }

    return new ResponseBuilder({
      email: email,
    })
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }

  public async resetPassword(
    request: ResetPasswordRequestDto,
  ): Promise<ResponsePayload<any>> {
    const { email, password, code } = request;
    const user = await this.userRepository.findOneByCondition({
      email: email,
    });

    if (!user) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.USER_NOT_FOUND'),
      ).toResponse();
    }

    const otp = new CheckOtpRequestDto();
    otp.email = email;
    otp.code = code;

    try {
      const otpValid = await this.checkOtp(otp);
      if (otpValid.statusCode !== ResponseCodeEnum.SUCCESS) {
        return otpValid;
      }
      const saltOrRounds = new ConfigService().get('saltOrRounds');
      user.password = await bcrypt.hash(password, saltOrRounds);

      await this.userRepository.update(user);

      return new ResponseBuilder().withCode(ResponseCodeEnum.SUCCESS).build();
    } catch (error) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(error?.message || error)
        .build();
    }
  }

  async refreshToken(payload: any): Promise<any> {
    const { id, rememberPassword } = payload;
    const user = await this.userRepository.findOneByCondition({
      id: id,
    });

    if (!user) {
      return new ApiError(
        ResponseCodeEnum.UNAUTHORIZED,
        await this.i18n.translate('error.WRONG_USERNAME_PASSWORD'),
      ).toResponse();
    }

    const accessToken = this._createToken(user.id, user.code, user.username);

    const refreshToken = this._createRefreshToken(user.id, rememberPassword);

    const response = plainToInstance(LoginResponseDto, {
      user,
      accessToken,
      refreshToken,
      rememberPassword,
    });

    return new ResponseBuilder(response)
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }

  async validateToken(payload: any): Promise<any> {
    const { jwt } = payload;
    try {
      console.log('jwt', jwt);

      this.jwtService.verify(jwt);
      const decodedUser: any = this.jwtService.decode(jwt);
      console.log('decoded user', decodedUser);

      const user = await this.userRepository.findOneByCondition({
        id: decodedUser?.id,
      });
      if (isEmpty(user)) {
        return new ApiError(ResponseCodeEnum.UNAUTHORIZED).toResponse();
      }
      return new ResponseBuilder(user)
        .withCode(ResponseCodeEnum.SUCCESS)
        .build();
    } catch (error) {
      console.log('error', error);

      if (error.constructor.name === 'TokenExpiredError') {
        return new ApiError(
          ResponseCodeEnum.TOKEN_EXPIRED,
          error.message,
        ).toResponse();
      }
      return new ApiError(
        ResponseCodeEnum.UNAUTHORIZED,
        error.message,
      ).toResponse();
    }
  }

  _createToken(id: number, code: string, username: string) {
    const expiresIn = jwtConstants.accessTokenExpiresIn || 60;
    const token = this.jwtService.sign(
      {
        id: id,
        code: code,
        username: username,
      },
      {
        expiresIn: `${expiresIn}s`,
      },
    );
    return {
      expiresIn: `${expiresIn}s`,
      token,
    };
  }
  _createRefreshToken(id: number, rememberPassword?: number) {
    let expiresIn = jwtConstants.refreshTokenExpiresIn || 60;
    if (rememberPassword && rememberPassword === RememberPassword.active) {
      expiresIn = jwtConstants.refreshTokenExpiresIn || 432000;
    }
    const token = this.jwtService.sign(
      {
        id: id,
      },
      {
        expiresIn: `${expiresIn}s`,
      },
    );
    return {
      expiresIn: `${expiresIn}s`,
      token,
    };
  }

  async login(payload: LoginRequestDto): Promise<ResponsePayload<any>> {
    const { password, username, rememberPassword } = payload;
    const user = await this.userRepository.validateUser(username, password);

    if (!user) {
      return new ApiError(
        ResponseCodeEnum.UNAUTHORIZED,
        await this.i18n.translate('error.WRONG_USERNAME_PASSWORD'),
      ).toResponse();
    }
    const userDetails = await this.userRepository.findOneByCondition({
      id: user.id,
    });

    const accessToken = this._createToken(
      userDetails.id,
      userDetails.code,
      userDetails.username,
    );

    const refreshToken = this._createRefreshToken(
      userDetails.id,
      rememberPassword,
    );

    const response = plainToInstance(LoginResponseDto, {
      userDetails,
      accessToken,
      refreshToken,
      rememberPassword,
    });

    return new ResponseBuilder(response)
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }
}
