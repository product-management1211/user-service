import { Filter } from "@utils/pagination.query";
import { WarehouseResponseDto } from "../dto/response/warehouse.response.dto";

export interface WarehouseServiceInterface {
  getListByIDs(idList: []): Promise<any>;
  getWarehousesByConditions(
    conditions: any,
    serilize?: boolean,
  ): Promise<WarehouseResponseDto | any>;
  getWarehousesByName(filterByWarehouseName: Filter, onlyId?: boolean): Promise<any>;
  getWarehousesUsingQuery(query: string);
}
