import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { escapeCharForSearch } from '@utils/common';
import { Filter } from '@utils/pagination.query';
import { isEmpty, map, uniq } from 'lodash';
import { lastValueFrom } from 'rxjs';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import { WarehouseServiceInterface } from './interface/warehouse.service.interface';

@Injectable()
export class WarehouseService implements WarehouseServiceInterface {
  constructor(
    @Inject('WAREHOUSE_SERVICE_CLIENT')
    private readonly warehouseServiceClient: ClientProxy,
  ) {}
  async getWarehousesUsingQuery(query: string) {
    const response = await lastValueFrom(
      this.warehouseServiceClient.send('get_warehouses_using_query', { query }),
    );
  }

  async getWarehousesByName(
    filterByWarehouseName: Filter,
    onlyId?: boolean,
  ): Promise<any> {
    const query =
      `SELECT * FROM warehouses ` +
      `WHERE LOWER(name) LIKE ` +
      `LOWER(unaccent('%${escapeCharForSearch(
        filterByWarehouseName.text,
      )}%')) ` +
      `ESCAPE '\\'`;
    const warehouses = await lastValueFrom(
      this.warehouseServiceClient.send('get_warehouses_using_query', { query }),
    );

    if (!isEmpty(warehouses) && onlyId === true) {
      return uniq(map(warehouses, 'id'));
    }

    return warehouses;
  }

  async getWarehousesByConditions(
    conditions: any,
    serilize?: boolean,
  ): Promise<any> {
    try {
      const response = await lastValueFrom(
        this.warehouseServiceClient.send(
          'get_warehouses_by_conditions',
          conditions,
        ),
      );
      return response;
    } catch (error) {
      console.log(error);
    }
  }

  async getListByIDs(ids: number[], relation?: string[]): Promise<any> {
    try {
      const response = await lastValueFrom(
        this.warehouseServiceClient.send('get_warehouses_by_ids', {
          warehouseIds: ids,
          relation,
        }),
      );

      if (response.statusCode !== ResponseCodeEnum.SUCCESS) {
        return [];
      }
      return response.data;
    } catch (err) {
      console.log(err);
      return [];
    }
  }
}
