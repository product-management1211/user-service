import { ApiProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty } from 'class-validator';
import { CompanyRequestDto } from './company.request.dto';

export class UpdateCompanyRequestDto extends CompanyRequestDto {
  @ApiProperty({ example: 1, description: '' })
  @IsInt()
  @IsNotEmpty()
  id: number;
}
