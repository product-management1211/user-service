import { BaseInterfaceRepository } from 'src/core/repository/base.interface.repository';
import { Company } from 'src/entities/company/company.entity';
import { CompanyRequestDto } from '../dto/request/company.request.dto';
import { GetCompaniesRequestDto } from '../dto/request/get-companies.request.dto';

export interface CompanyRepositoryInterface
  extends BaseInterfaceRepository<Company> {
  createEntity(companyRequestDto: CompanyRequestDto): Company;
  getCompanies(request: GetCompaniesRequestDto);
}
