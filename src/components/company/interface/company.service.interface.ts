import { ResponsePayload } from 'src/utils/response-payload';
import { CompanyRequestDto } from '../dto/request/company.request.dto';
import { GetCompaniesRequestDto } from '../dto/request/get-companies.request.dto';
import { SetCompanyStatusRequestDto } from '../dto/request/set-company-status.request.dto';
import { UpdateCompanyRequestDto } from '../dto/request/update-company.request.dto';
import { CompanyResponseDto } from '../dto/response/company.response.dto';
import { GetCompaniesResponseDto } from '../dto/response/get-companies.response.dto';

export interface CompanyServiceInterface {
  create(
    companyRequestDto: CompanyRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>>;
  update(
    companyDto: UpdateCompanyRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>>;
  delete(id: number): Promise<ResponsePayload<any>>;
  getDetail(id: number): Promise<ResponsePayload<CompanyResponseDto | any>>;
  getList(
    request: GetCompaniesRequestDto,
  ): Promise<ResponsePayload<GetCompaniesResponseDto | any>>;
  confirm(
    request: SetCompanyStatusRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>>;
  reject(
    request: SetCompanyStatusRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>>;
  getCompanyById(id: number): Promise<CompanyResponseDto | any>;
}
