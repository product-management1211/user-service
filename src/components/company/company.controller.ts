import { Body, Controller, Inject } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { isEmpty } from 'lodash';
import isEmptyObject from 'src/utils/helper';
import { ResponsePayload } from 'src/utils/response-payload';
import { CompanyRequestDto } from './dto/request/company.request.dto';
import { GetCompaniesRequestDto } from './dto/request/get-companies.request.dto';
import { SetCompanyStatusRequestDto } from './dto/request/set-company-status.request.dto';
import { UpdateCompanyRequestDto } from './dto/request/update-company.request.dto';
import { CompanyResponseDto } from './dto/response/company.response.dto';
import { GetCompaniesResponseDto } from './dto/response/get-companies.response.dto';
import { CompanyServiceInterface } from './interface/company.service.interface';

@Controller('companies')
export class CompanyController {
  constructor(
    @Inject('CompanyServiceInterface')
    private readonly companyService: CompanyServiceInterface,
  ) {}

  @MessagePattern('company_create')
  public async create(
    @Body() payload: CompanyRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }

    return await this.companyService.create(request);
  }

  @MessagePattern('company_update')
  public async update(
    @Body() payload: UpdateCompanyRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.companyService.update(request);
  }

  @MessagePattern('company_delete')
  public async delete(id: number): Promise<ResponsePayload<any>> {
    return await this.companyService.delete(id);
  }

  @MessagePattern('company_detail')
  public async getDetail(
    id: number,
  ): Promise<ResponsePayload<CompanyResponseDto | any>> {
    return await this.companyService.getDetail(id);
  }

  @MessagePattern('company_list')
  public async getList(
    @Body() payload: GetCompaniesRequestDto,
  ): Promise<ResponsePayload<GetCompaniesResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.companyService.getList(request);
  }

  @MessagePattern('confirm_company')
  public async confirm(
    @Body() payload: SetCompanyStatusRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.companyService.confirm(request);
  }

  @MessagePattern('reject_company')
  public async reject(
    @Body() payload: SetCompanyStatusRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>> {
    const { request, responseError } = payload;
    if (responseError && !isEmpty(responseError)) {
      return responseError;
    }
    return await this.companyService.reject(request);
  }

  @MessagePattern('get_company_by_id')
  public async getCompanyByCondition(
    request,
  ): Promise<ResponsePayload<CompanyResponseDto | any>> {
    return await this.companyService.getCompanyById(request.id);
  }
}
