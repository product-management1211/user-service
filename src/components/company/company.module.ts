import { UserModule } from '@components/user/user.module';
import { UserService } from '@components/user/user.service';
import { WarehouseModule } from '@components/warehouse/warehouse.module';
import { WarehouseService } from '@components/warehouse/warehouse.service';
import { Factory } from '@entities/factory/factory.entity';
import { User } from '@entities/user/user.entity';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Company } from 'src/entities/company/company.entity';
import { CompanyRepository } from 'src/repository/company.repository';
import { FactoryRepository } from 'src/repository/factory.repository';
import { UserRepository } from 'src/repository/user.repository';
import { CompanyController } from './company.controller';
import { CompanyService } from './company.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Company, Factory, User]),
    WarehouseModule,
    UserModule,
  ],
  providers: [
    {
      provide: 'CompanyRepositoryInterface',
      useClass: CompanyRepository,
    },
    {
      provide: 'CompanyServiceInterface',
      useClass: CompanyService,
    },
    {
      provide: 'FactoryRepositoryInterface',
      useClass: FactoryRepository,
    },
    {
      provide: 'WarehouseServiceInterface',
      useClass: WarehouseService,
    },
    // {
    //   provide: 'UserServiceInterface',
    //   useClass: UserService,
    // },
    {
      provide: 'UserRepositoryInterface',
      useClass: UserRepository,
    },
  ],
  controllers: [CompanyController],
})
export class CompanyModule {}
