import { FactoryRepositoryInterface } from '@components/factory/interface/factory.repository.interface';
import { UserRepositoryInterface } from '@components/user/interface/user.repository.interface';
import { WarehouseServiceInterface } from '@components/warehouse/interface/warehouse.service.interface';
import { Inject, Injectable } from '@nestjs/common';
import { ApiError } from '@utils/api.error';
import { serialize } from '@utils/helper';
import { PagingResponse } from '@utils/paging.response';
import { plainToInstance } from 'class-transformer';
import { map, uniq } from 'lodash';
import { I18nService } from 'nestjs-i18n';
import { ResponseCodeEnum } from 'src/constants/response-code.enum';
import { Company } from 'src/entities/company/company.entity';
import { ResponseBuilder } from 'src/utils/response-builder';
import { ResponsePayload } from 'src/utils/response-payload';
import { In, Not } from 'typeorm';
import {
  CAN_DELETE_COMPANY_STATUS,
  CAN_UPDATE_COMPANY_STATUS,
  CompanyStatusEnum,
} from './company.constant';
import { CompanyRequestDto } from './dto/request/company.request.dto';
import { GetCompaniesRequestDto } from './dto/request/get-companies.request.dto';
import { SetCompanyStatusRequestDto } from './dto/request/set-company-status.request.dto';
import { UpdateCompanyRequestDto } from './dto/request/update-company.request.dto';
import {
  CompaniesResponseDto,
  CompanyDto,
  CompanyResponseDto,
} from './dto/response/company.response.dto';
import { CompanyRepositoryInterface } from './interface/company.repository.interface';
import { CompanyServiceInterface } from './interface/company.service.interface';

@Injectable()
export class CompanyService implements CompanyServiceInterface {
  constructor(
    @Inject('CompanyRepositoryInterface')
    private companyRepository: CompanyRepositoryInterface,

    @Inject('WarehouseServiceInterface')
    private readonly warehouseService: WarehouseServiceInterface,

    @Inject('FactoryRepositoryInterface')
    private readonly factoryRepository: FactoryRepositoryInterface,

    @Inject('UserRepositoryInterface')
    private readonly userRepository: UserRepositoryInterface,

    private readonly i18n: I18nService,
  ) {}
  public async getDetail(id: number): Promise<ResponsePayload<any>> {
    const company = await this.companyRepository.findOneByCondition({
      id,
    });
    const companyDetails = { ...company, approver: {} };
    if (company.approverId !== null) {
      const approver = await this.userRepository.findByCondition({
        id: company.approverId,
      });
      companyDetails.approver = approver;
    }

    const response = plainToInstance(CompanyDto, companyDetails, {
      excludeExtraneousValues: true,
    });

    return new ResponseBuilder()
      .withCode(ResponseCodeEnum.SUCCESS)
      .withData(response)
      .build();
  }

  public async getList(
    request: GetCompaniesRequestDto,
  ): Promise<ResponsePayload<any>> {
    const { result, count } = await this.companyRepository.getCompanies(
      request,
    );

    const approverIds = uniq(map(result, 'approverId'));
    const approvers = await this.userRepository.findByCondition({
      id: In(approverIds),
    });
    const serilizedApprovers = approvers ? serialize(approvers) : [];

    const companies = result.map((company) => ({
      ...company,
      approver: serilizedApprovers[company.approverId] || {},
    }));

    const response = plainToInstance(CompanyDto, companies, {
      excludeExtraneousValues: true,
    });

    return new ResponseBuilder<PagingResponse>({
      items: response,
      meta: { total: count, page: request.page },
    })
      .withCode(ResponseCodeEnum.SUCCESS)
      .build();
  }
  public async confirm(
    request: SetCompanyStatusRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>> {
    const { userId, id } = request;

    const company = await this.companyRepository.findOneByCondition({ id });

    if (!company) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.COMPANY_NOT_FOUND'))
        .build();
    }

    company.approverId = userId;
    company.approvedAt = new Date(Date.now());
    company.status = CompanyStatusEnum.CONFIRMED;
    console.log('company updated', company);

    const result = await this.companyRepository.save(company);
    const response = plainToInstance(CompanyResponseDto, result, {
      excludeExtraneousValues: true,
    });
    return new ResponseBuilder(response)
      .withCode(ResponseCodeEnum.SUCCESS)
      .withMessage(await this.i18n.translate('success.SUCCESS'))
      .build();
  }

  public async reject(
    request: SetCompanyStatusRequestDto,
  ): Promise<ResponsePayload<CompanyResponseDto | any>> {
    const { userId, id } = request;
    const company = await this.companyRepository.findOneByCondition({ id });

    if (!company) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.COMPANY_NOT_FOUND'))
        .build();
    }

    if (company.status === CompanyStatusEnum.CONFIRMED) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.COMPANY_WAS_CONFIRMED'),
      ).toResponse();
    }

    company.approverId = userId;
    company.approvedAt = new Date(Date.now());
    company.status = CompanyStatusEnum.REJECT;
    const result = await this.companyRepository.save(company);
    const response = plainToInstance(CompanyResponseDto, result, {
      excludeExtraneousValues: true,
    });
    return new ResponseBuilder(response)
      .withCode(ResponseCodeEnum.SUCCESS)
      .withMessage(await this.i18n.translate('success.SUCCESS'))
      .build();
  }

  public async delete(id: number): Promise<ResponsePayload<any>> {
    const company = await this.companyRepository.findOneByCondition({
      id: id,
      deletedAt: null,
    });

    if (!company) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.COMPANY_NOT_FOUND'))
        .build();
    }

    if (!CAN_DELETE_COMPANY_STATUS.includes(company.status)) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.COMPANY_WAS_CONFIRMED'),
      ).toResponse();
    }

    const factories = await this.factoryRepository.findByCondition({
      companyId: id,
    });

    if (factories.length > 0) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.MUST_DELETE_FACTORY'),
        { factoryIds: map(factories, 'id') },
      ).toResponseWithData();
    }

    const companyWarehouse =
      await this.warehouseService.getWarehousesByConditions({
        companyId: id,
      });

    if (companyWarehouse.length > 0) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.MUST_DELETE_WAREHOUSE'),
        { warehouseIds: map(companyWarehouse, 'id') },
      ).toResponseWithData();
    }

    try {
      await this.companyRepository.delete(id);
      return new ResponseBuilder().withCode(ResponseCodeEnum.SUCCESS).build();
    } catch (error) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.CAN_NOT_DELETE'),
      ).toResponse();
    }
  }

  public async update(
    companyDto: UpdateCompanyRequestDto,
  ): Promise<ResponsePayload<any>> {
    const company = await this.companyRepository.findOneByCondition({
      id: companyDto.id,
    });

    if (!company) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.COMPANY_NOT_FOUND'))
        .build();
    }

    if (!CAN_UPDATE_COMPANY_STATUS.includes(company.status)) {
      return new ApiError(
        ResponseCodeEnum.BAD_REQUEST,
        await this.i18n.translate('error.COMPANY_WAS_CONFIRMED'),
      ).toResponse();
    }

    const codeCondition = [{ code: companyDto.code, id: Not(companyDto.id) }];
    const checkExistCode = await this.checkUniqueCompany(codeCondition);

    if (checkExistCode) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.CODE_ALREADY_EXISTS'))
        .build();
    }

    company.code = companyDto.code;
    company.name = companyDto.name;
    company.address = companyDto.address;
    company.phone = companyDto.phone;
    company.taxNo = companyDto.taxNo;
    company.email = companyDto.email;
    company.fax = companyDto.fax;
    company.description = companyDto.description;
    company.status = CompanyStatusEnum.CREATED;
    company.approverId = null;
    company.approvedAt = null;

    return await this.save(company);
  }

  public async getCompanyById(id: number): Promise<CompanyResponseDto | any> {
    const company = await this.companyRepository.findOneByCondition({ id });
    const response = plainToInstance(CompanyDto, company, {
      excludeExtraneousValues: true,
    });

    return new ResponseBuilder()
      .withCode(ResponseCodeEnum.SUCCESS)
      .withData(response)
      .build();
  }

  public async create(
    companyRequestDto: CompanyRequestDto,
  ): Promise<ResponsePayload<any>> {
    const existedCompanyCode = await this.checkUniqueCompany({
      code: companyRequestDto.code,
    });
    if (existedCompanyCode) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.BAD_REQUEST)
        .withMessage(await this.i18n.translate('error.CODE_ALREADY_EXISTS'))
        .build();
    }

    const companyEntity = await this.companyRepository.createEntity(
      companyRequestDto,
    );

    return await this.save(companyEntity);
  }

  private async checkUniqueCompany(condition: any): Promise<boolean> {
    const result = await this.companyRepository.findByCondition(condition);
    return result.length > 0;
  }

  private async save(
    companyEntity: Company,
  ): Promise<ResponsePayload<CompanyResponseDto | any>> {
    try {
      const result = await this.companyRepository.save(companyEntity);
      const response = plainToInstance(CompanyResponseDto, result, {
        excludeExtraneousValues: true,
      });
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.SUCCESS)
        .withData(response)
        .build();
    } catch (error) {
      return new ResponseBuilder()
        .withCode(ResponseCodeEnum.INTERNAL_SERVER_ERROR)
        .withMessage(error?.message || error)
        .build();
    }
  }
}
