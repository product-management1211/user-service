import { UserRoleSettingRepositoryInterface } from '@components/settings/user-role-setting/interface/user-role-setting.repository.interface';
import { BaseAbstractRepository } from '@core/repository/base.abstract.repository';
import { UserRoleSetting } from '@entities/user-role-setting/user-role-setting.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

export class UserRoleSettingRepository
  extends BaseAbstractRepository<UserRoleSetting>
  implements UserRoleSettingRepositoryInterface
{
  constructor(
    @InjectRepository(UserRoleSetting)
    private readonly userRoleSettingRepository: Repository<UserRoleSetting>,
  ) {
    super(userRoleSettingRepository);
  }
}
