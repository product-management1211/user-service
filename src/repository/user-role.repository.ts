import { UserRoleRepositoryInterface } from '@components/user-role/interface/user-role.repository.interface';
import { BaseAbstractRepository } from '@core/repository/base.abstract.repository';
import { UserRole } from '@entities/user-role/user-role.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UserRoleRepository
  extends BaseAbstractRepository<UserRole>
  implements UserRoleRepositoryInterface
{
  constructor(
    @InjectRepository(UserRole)
    private readonly userRolesRepository: Repository<UserRole>,
  ) {
    super(userRolesRepository);
  }
}
