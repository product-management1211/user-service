import { CompanyRequestDto } from '@components/company/dto/request/company.request.dto';
import { GetCompaniesRequestDto } from '@components/company/dto/request/get-companies.request.dto';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { escapeCharForSearch } from '@utils/common';
import { isEmpty } from 'lodash';
import { CompanyRepositoryInterface } from 'src/components/company/interface/company.repository.interface';
import { BaseAbstractRepository } from 'src/core/repository/base.abstract.repository';
import { Company } from 'src/entities/company/company.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CompanyRepository
  extends BaseAbstractRepository<Company>
  implements CompanyRepositoryInterface
{
  constructor(
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
  ) {
    super(companyRepository);
  }
  public async getCompanies(request: GetCompaniesRequestDto) {
    const { keyword, sort, filter, limit, page } = request;
    let query = this.companyRepository.createQueryBuilder('c');

    if (keyword) {
      query = query
        .orWhere(`LOWER("c"."name") LIKE LOWER(:keywordParam) escape '\\'`, {
          keywordParam: `%${escapeCharForSearch(keyword)}%`,
        })
        .orWhere(`LOWER("c"."code") LIKE LOWER(:keywordParam) escape '\\'`, {
          keywordParam: `%${escapeCharForSearch(keyword)}%`,
        });
    }

    if (filter) {
      console.log('filter', filter.length);

      filter.forEach((item) => {
        console.log(item);

        query = query.andWhere(
          `LOWER("c".${item.column}) LIKE LOWER(:filterParam) escape '\\'`,
          {
            filterParam: `%${escapeCharForSearch(item.text)}%`,
          },
        );
      });
    }

    if (!isEmpty(sort)) {
      sort.forEach((item) => {
        query = query.addOrderBy(item.column, item.order);
      });
    } else {
      query = query.orderBy('c.id', 'DESC');
    }

    const [result, count] = await query
      .offset((page - 1) * limit)
      .limit(limit)
      .getManyAndCount();

    return { result, count };
  }

  public createEntity(companyDto: CompanyRequestDto): Company {
    const companyEntity = new Company();

    companyEntity.code = companyDto.code;
    companyEntity.name = companyDto.name;
    companyEntity.address = companyDto.address;
    companyEntity.phone = companyDto.phone;
    companyEntity.taxNo = companyDto.taxNo;
    companyEntity.email = companyDto.email;
    companyEntity.fax = companyDto.fax;
    companyEntity.description = companyDto.description;

    return companyEntity;
  }
}
