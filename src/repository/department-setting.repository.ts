import { DepartmentSettingRepositoryInterface } from '@components/settings/department-setting/interface/department-setting.repository.interface';
import { BaseAbstractRepository } from '@core/repository/base.abstract.repository';
import { DepartmentSettings } from '@entities/department-setting/department-setting.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class DepartmentSettingRepository
  extends BaseAbstractRepository<DepartmentSettings>
  implements DepartmentSettingRepositoryInterface
{
  constructor(
    @InjectRepository(DepartmentSettings)
    private readonly departmentSettingRepository: Repository<DepartmentSettings>,
  ) {
    super(departmentSettingRepository);
  }
}
