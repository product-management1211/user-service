import { CreateFactoryRequestDto } from '@components/factory/dto/request/create-factory.request.dto';
import { GetFactoriesRequestDto } from '@components/factory/dto/request/get-factories.request.dto';
import { FactoryRepositoryInterface } from '@components/factory/interface/factory.repository.interface';
import { BaseAbstractRepository } from '@core/repository/base.abstract.repository';
import { Factory } from '@entities/factory/factory.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { escapeCharForSearch } from '@utils/common';
import { isEmpty } from 'lodash';
import { Repository } from 'typeorm';

@Injectable()
export class FactoryRepository
  extends BaseAbstractRepository<Factory>
  implements FactoryRepositoryInterface
{
  constructor(
    @InjectRepository(Factory)
    private readonly factoryRepository: Repository<Factory>,
  ) {
    super(factoryRepository);
  }
  public async getList(request: GetFactoriesRequestDto) {
    const { keyword, sort, filter, take, skip, isGetAll } = request;

    let query = this.factoryRepository
      .createQueryBuilder('f')
      .select([
        'f.id AS id',
        'f.company_id AS "companyId"',
        'c.name AS "companyName"',
        'f.name AS "name"',
        'f.code AS "code"',
        'f.location AS "location"',
        'f.phone AS "phone"',
        'f.description AS "description"',
        'f.status AS "status"',
        'f.approver_id AS "approverId"',
        'f.created_at AS "createdAt"',
        'f.updated_at AS "updatedAt"',
      ])
      .leftJoin('companies', 'c', 'c.id = f.company_id');

    if (keyword) {
      query = query
        .orWhere(`LOWER("f"."name") LIKE LOWER(:keywordParam) escape '\\'`, {
          keywordParam: `%${escapeCharForSearch(keyword)}%`,
        })
        .orWhere(`LOWER("f"."code") LIKE LOWER(:keywordParam) escape '\\'`, {
          keywordParam: `%${escapeCharForSearch(keyword)}%`,
        });
    }

    if (!isEmpty(filter)) {
      filter.forEach((item) => {
        switch (item.column) {
          case 'code':
            query.andWhere(`LOWER("f"."code") LIKE LOWER(:code) escape '\\'`, {
              code: `%${escapeCharForSearch(item.text)}%`,
            });
            break;
          case 'name':
            query.andWhere(`LOWER("f"."name") LIKE LOWER(:name) escape '\\'`, {
              name: `%${escapeCharForSearch(item.text)}%`,
            });
            break;
          case 'companyName':
            query.andWhere(
              `LOWER("c"."name") LIKE LOWER(:companyName) escape '\\'`,
              {
                companyName: `%${escapeCharForSearch(item.text)}%`,
              },
            );
            break;
          default:
            break;
        }
      });
    }
    if (!isEmpty(sort)) {
      sort.forEach((item) => {
        switch (item.column) {
          case 'code':
            query.addOrderBy('f.code', item.order);
            break;
          case 'name':
            query.addOrderBy('f.name', item.order);
            break;
          case 'companyName':
            query.addOrderBy('c.name', item.order);
            break;
          default:
            break;
        }
      });
    } else {
      query = query.orderBy('f.id', 'DESC');
    }
    console.log('skip', skip);

    const result = parseInt(isGetAll)
      ? await query.withDeleted().getRawMany()
      : await query.offset(skip).limit(take).getRawMany();

    const count = await query.getCount();

    return {
      result,
      count,
    };
  }
  public createEntity(factoryRequestDto: CreateFactoryRequestDto): Factory {
    const factoryEntity = new Factory();

    factoryEntity.companyId = factoryRequestDto.companyId;
    factoryEntity.name = factoryRequestDto.name;
    factoryEntity.code = factoryRequestDto.code;
    factoryEntity.description = factoryRequestDto.description;
    factoryEntity.phone = factoryRequestDto.phone;
    factoryEntity.location = factoryRequestDto.location;

    return factoryEntity;
  }
}
