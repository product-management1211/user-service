import { UserDepartment } from '@entities/user-department/user-department.entity';
import { UserFactory } from '@entities/user-factory/user-factory.entity';
import { UserRole } from '@entities/user-role/user-role.entity';
import { UserWarehouse } from '@entities/user-warehouse/user-warehouse.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { escapeCharForSearch } from '@utils/common';
import { isEmpty } from 'lodash';
import { CreateUserRequestDto } from 'src/components/user/dto/request/create-user.request.dto';
import { UserRepositoryInterface } from 'src/components/user/interface/user.repository.interface';
import { BaseAbstractRepository } from 'src/core/repository/base.abstract.repository';
import { User } from 'src/entities/user/user.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { SUPER_ADMIN } from 'src/constants/common';

@Injectable()
export class UserRepository
  extends BaseAbstractRepository<User>
  implements UserRepositoryInterface
{
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
  ) {
    super(usersRepository);
  }
  isSuperAdmin(code: string): boolean {
    return code === SUPER_ADMIN.code;
  }

  public async getDetail(id: number): Promise<any> {
    const user = await this.usersRepository.findOne({
      relations: [
        'departmentSettings',
        'userRoleSettings',
        'factories',
        'userWarehouses',
      ],
      where: { id: id },
    });

    return user;
  }

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersRepository
      .createQueryBuilder('u')
      .select(['u.id as id', 'u.password as password'])
      .where('username = :username', { username: username })
      .getRawOne();

    if (!user) return false;
    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) return false;

    return user;
  }

  async getUsers(request: any, warehouseIds?: number[]): Promise<any> {
    console.log('request', request);
    const { skip, take, keyword, sort, filter, isGetAll } = request;
    let query = this.usersRepository
      .createQueryBuilder('u')
      .select([
        'u.id AS id',
        'u.full_name AS "fullName"',
        'u.code AS "code"',
        'u.email AS "email"',
        'u.username AS "username"',
        'u.company_id AS "companyId"',
        `CASE WHEN COUNT(uw) = 0 THEN '[]' ELSE JSON_AGG(
          DISTINCT JSONB_BUILD_OBJECT('id', uw.warehouseId)) END AS "userWarehouses"`,
        `CASE WHEN COUNT(f) = 0 THEN '[]' ELSE JSON_AGG(
            DISTINCT JSONB_BUILD_OBJECT('id', f.id, 'name', f.name)) END AS "factories"`,
        `CASE WHEN COUNT(urs) = 0 THEN '[]' ELSE JSON_AGG(
            DISTINCT JSONB_BUILD_OBJECT('id', urs.id, 'name', urs.name, 'code', urs.code)) END AS "userRoleSettings"`,
        `CASE WHEN COUNT(ds) = 0 THEN '[]' ELSE JSON_AGG(
            DISTINCT JSONB_BUILD_OBJECT('id', ds.id, 'name', ds.name)) END AS "departmentSettings"`,
      ])
      .leftJoin('u.userRoleSettings', 'urs')
      .leftJoin('u.departmentSettings', 'ds')
      .leftJoin('u.userWarehouses', 'uw')
      .leftJoin('u.factories', 'f');

    if (!isEmpty(keyword)) {
      query
        .orWhere(`LOWER("u"."username") LIKE LOWER(:pKeyword) escape '\\'`, {
          pKeyword: `%${escapeCharForSearch(keyword)}%`,
        })
        .orWhere(`LOWER("u"."full_name") LIKE LOWER(:pKeyword) escape '\\'`, {
          pKeyword: `%${escapeCharForSearch(keyword)}%`,
        });
    }

    if (!isEmpty(filter)) {
      console.log('filter', filter);
      filter.forEach((item) => {
        switch (item.column) {
          case 'fullName':
            query.andWhere(
              `LOWER("u"."full_name") LIKE lower(:fullName) escape '\\'`,
              {
                fullName: `%${escapeCharForSearch(item.text)}%`,
              },
            );
            break;
          case 'username':
            query.andWhere(
              `lower("u"."username") like lower(:username) escape '\\'`,
              {
                username: `%${escapeCharForSearch(item.text)}%`,
              },
            );
            break;
          case 'departmentName':
            console.log('item', item.text);
            query.andWhere(
              `lower("ds"."name") like lower(:departmentSetting) escape '\\'`,
              {
                departmentSetting: `%${escapeCharForSearch(item.text)}%`,
              },
            );
            break;
          case 'roleName':
            query.andWhere(
              `lower("urs"."name") like lower(:userRoleSetting) escape '\\'`,
              {
                userRoleSetting: `%${escapeCharForSearch(item.text)}%`,
              },
            );
            break;
          default:
            break;
        }
      });
    }

    if (!isEmpty(sort)) {
      sort.forEach((item) => {
        switch (item.column) {
          case 'username':
            query.orderBy('"u"."username"', item.order);
            break;
          case 'fullName':
            query = query.orderBy('"u"."full_name"', item.order);
            break;
          default:
            break;
        }
      });
    } else {
      query.orderBy('"u"."id"', 'DESC');
    }

    if (!isEmpty(warehouseIds)) {
      console.log('warehouseIds', warehouseIds);

      query.andWhere((q) => {
        const subQuery = q
          .subQuery()
          .select('uw.userId')
          .distinct()
          .from(UserWarehouse, 'uw')
          .where('uw.warehouseId IN (:...warehouseIds)', {
            warehouseIds: warehouseIds,
          });
        return `u.id IN ${subQuery.getQuery()}`;
      });
    }

    query.groupBy('u.id');

    const users = parseInt(isGetAll)
      ? await query.withDeleted().getRawMany()
      : await query.offset(skip).limit(take).getRawMany();
    const count = await query.getCount();
    return {
      data: users,
      count: count,
    };
  }

  createUserWarehouseEntity(
    userId: number,
    warehouseId: number,
  ): UserWarehouse {
    const userWarehouse = new UserWarehouse();

    userWarehouse.warehouseId = warehouseId;
    userWarehouse.userId = userId;

    return userWarehouse;
  }

  createUserRoleEntity(
    userId: number,
    roleId: number,
    departmentId: number,
  ): UserRole {
    const userRole = new UserRole();
    userRole.userId = userId;
    userRole.userRoleId = roleId;
    userRole.departmentId = departmentId;
    return userRole;
  }

  createUserDepartmentEntity(
    userId: number,
    departmentId: number,
  ): UserDepartment {
    const userDepartment = new UserDepartment();
    userDepartment.userId = userId;
    userDepartment.departmentId = departmentId;
    return userDepartment;
  }

  createUserFactoryEntity(userId: number, factoryId: number): UserFactory {
    const userFactory = new UserFactory();
    userFactory.factoryId = factoryId;
    userFactory.userId = userId;
    return userFactory;
  }

  async checkUniqueUser(condition: any): Promise<any> {
    return await this.usersRepository.find({
      where: condition,
      select: ['id'],
    });
  }

  public createEntity(userDto: CreateUserRequestDto): User {
    const user = new User();

    user.email = userDto.email;
    user.username = userDto.username;
    user.fullName = userDto.fullName;
    user.password = userDto.password;
    user.companyId = userDto.companyId;
    user.dateOfBirth = userDto.dateOfBirth;
    user.code = userDto.code;
    user.phone = userDto.phone;

    return user;
  }
}
