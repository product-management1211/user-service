import * as dotenv from 'dotenv';
import { DataSourceOptions } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

dotenv.config();

const ormConfig = {
  type: 'postgres',
  host: process.env.DATABASE_HOST,
  port: parseInt(process.env.DATABASE_PORT),
  maxPool: parseInt(process.env.DATABASE_MAX_POOL) || 20,
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  logging: process.env.ENV === 'development',
};

const connectionOptions: DataSourceOptions = {
  type: 'postgres',
  host: ormConfig.host,
  port: ormConfig.port,
  username: ormConfig.username,
  password: ormConfig.password,
  database: ormConfig.database,
  entities: ['dist/entities/**/*.entity.{ts,js}', 'src/entities/*.{ts,js}'],
  migrations: ['dist/database/migrations/*.{ts,js}'],
  subscribers: ['dist/observers/subscribers/*.subscribers.{ts,js}'],
  synchronize: false,
  migrationsRun: false,
  extra: {
    maxPool: ormConfig.maxPool,
  },
  namingStrategy: new SnakeNamingStrategy(),
};

export = connectionOptions;
