import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { I18nModule } from 'nestjs-i18n';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './components/user/user.module';
import * as path from 'path';
import { CompanyModule } from './components/company/company.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as connectionOptions from '@config/database.config';
import { DataSource } from 'typeorm';
import { FactoryModule } from '@components/factory/factory.module';
import { AuthModule } from '@components/auth/auth.module';
import { APP_PIPE } from '@nestjs/core';
import { ValidationPipe } from '@core/pipe/validation.pipe';
import { MailModule } from '@components/mail/mail.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    I18nModule.forRoot({
      fallbackLanguage: 'vi',
      loaderOptions: {
        path: path.join(__dirname, '/i18n/'),
        watch: true,
      },
    }),
    TypeOrmModule.forRoot(connectionOptions),
    UserModule,
    CompanyModule,
    FactoryModule,
    AuthModule,
    MailModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
